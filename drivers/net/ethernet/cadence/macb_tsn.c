// SPDX-License-Identifier: GPL-2.0-only
/*
 * Phytium TSN support driver
 *
 * Copyright (C) 2023, Phytium Technology Co., Ltd.
 */
#include <linux/netdevice.h>
#include <linux/platform_device.h>
#include <linux/property.h>
#include <net/pkt_sched.h>
#include <linux/bitops.h>

#include "macb.h"
#include "macb_tsn.h"

static int phytium_set_cbs_tc(struct net_device *ndev, void *type_data)
{
	struct macb *bp = netdev_priv(ndev);
	struct tc_cbs_qopt_offload *cbs = type_data;
	u32 cbs_ctrl, idle_slope;
	u8 tc = cbs->queue;
	u8 tc_nums = netdev_get_num_tc(ndev);
	u8 prio_top, prio_next;

	prio_top = tc_nums - 1;
	prio_next = tc_nums - 2;

	pr_debug("%s %d cbs->queue %d\n", __func__, __LINE__, cbs->queue);
	pr_debug("%s %d tc_nums %d\n", __func__, __LINE__, tc_nums);

	/* CBS is available on the two highest priority active queues */
	if (tc != prio_top && tc != prio_next)
		return -EINVAL;

	/* idleslope should be positive, sendslope should be negative */
	if (cbs->idleslope < 0 || cbs->sendslope > 0)
		return -EINVAL;

	/* if set sendslope, check idle - send == speed */
	if (cbs->sendslope != 0 && (cbs->idleslope - cbs->sendslope) != bp->speed * 1000 * 1000) {
		pr_warn("idleslope - sendslope != portTransmitRate, please check it\n");
		return -EINVAL;
	}

	cbs_ctrl = gem_readl(bp, CBS_CONTROL);

	if (!cbs->enable) {
		cbs_ctrl &= (~ BIT(prio_top - tc));
		gem_writel(bp, CBS_CONTROL, cbs_ctrl);
		return 0;
	}

	pr_debug("bp->speed %d\n", bp->speed);
	pr_debug("cbs->idleslope %d\n", cbs->idleslope);

	/* calculate idle slope value, cbs->idleslope is bytes/sec       *
	 * reg value in bytes/sec for gigabit operation, and nibbles/sec *
	 * for 10/100 operation, and scaled by 2.5 in 2.5G operation     */
	switch(bp->speed) {
	case SPEED_10:
	case SPEED_100:
		idle_slope = cbs->idleslope * 2;
		break;
	case SPEED_1000:
		idle_slope = cbs->idleslope;
		break;
	case SPEED_2500:
		idle_slope = cbs->idleslope * 2 / 5;
		break;
	default:
		pr_info("macb speed %d not support\n", bp->speed);
		return -EOPNOTSUPP;
	}

	/* cbs should be disabled prior to updating the idle slope value */
	cbs_ctrl &= (~ BIT(prio_top - tc));
	gem_writel(bp, CBS_CONTROL, cbs_ctrl);

	if (tc == prio_top) {
		gem_writel(bp, CBS_IDLESLOPE_Q_A, idle_slope);
	}
	else {
		gem_writel(bp, CBS_IDLESLOPE_Q_B, idle_slope);
	}

	/* after set idle slope, enable cbs */
	cbs_ctrl |= BIT(prio_top - tc);
	gem_writel(bp, CBS_CONTROL, cbs_ctrl);

	return 0;
}

static int phytium_set_mqprio_tc(struct net_device *ndev, void *type_data)
{
	struct macb *bp = netdev_priv(ndev);
	struct tc_mqprio_qopt *mqprio = type_data;
	u8 num_tc;
	int i;

	pr_debug("%s %d\n", __func__, __LINE__);
	mqprio->hw = TC_MQPRIO_HW_OFFLOAD_TCS;
	num_tc = mqprio->num_tc;

	if (!num_tc) {
		netdev_reset_tc(ndev);
		return 0;
	}

	/* For easier use, num_tc need same as num_queues. */
	if (num_tc != bp->num_queues) {
		netdev_err(ndev, "num_tc should same as dev num_queues %d\n",
			   bp->num_queues);
		return -EINVAL;
	}

	netdev_set_num_tc(ndev, num_tc);

	/* Each TC is associated with one netdev queue, the queue index is *
	 * used as the priority, where a higher index will have a higher   *
	 * priority than a lower index.                                    */
	for (i = 0; i < num_tc; i++)
		netdev_set_tc_queue(ndev, i, 1, i);
	return 0;
}

struct queues_enst_setting {
	u8  enable;
	u32 start_time;
	u32 on_time;
	u32 off_time;
	u32 off_time_tmp;
};

#ifdef TSN_DEBUG
static void _dump_taprio_param(struct tc_taprio_qopt_offload *taprio)
{
	int i = 0;
	_dump_param(taprio->enable);
	_dump_param(taprio->base_time);
	_dump_param(taprio->cycle_time);
	_dump_param(taprio->cycle_time_extension);
	_dump_param(taprio->num_entries);
	for(; i < taprio->num_entries; i++) {
		_dump_param(i);
		_dump_param(taprio->entries[i].command);
		_dump_param(taprio->entries[i].gate_mask);
		_dump_param(taprio->entries[i].interval);
	}
}

static void _dump_enst_param(struct queues_enst_setting *setting)
{
	int i = 0;
	for(; i < MACB_MAX_ENST_QUEUES; i++) {
		if (setting[i].enable == false)
			continue;
		_dump_param(i);
		_dump_param(setting[i].enable);
		_dump_param(setting[i].start_time);
		_dump_param(setting[i].on_time);
		_dump_param(setting[i].off_time);
	}
}
#else
#define _dump_taprio_param(taprio)
#define _dump_enst_param(setting)
#endif

static int _taprio_convert_enst(struct tc_taprio_qopt_offload *taprio, struct queues_enst_setting *setting)
{
	int i, j;
	// TODO: queue start time need add base_time?
	u32 queue_start_time = 0;

	for (i = 0; i < taprio->num_entries; i++) {
		unsigned long bitmap = (unsigned long)taprio->entries[i].gate_mask;
		if(!bitmap) {
			pr_warn("gata_mask should not be zero\n");
			return -EINVAL;
		}
		for_each_set_bit(j, &bitmap, MACB_MAX_ENST_QUEUES) {
			/* find this entrie masked queue j */
			if (setting[j].enable == true) {
				pr_warn("not support setting same queue two times, hw assumes with fixed period\n");
				return -EINVAL;
			}
			setting[j].enable = true;
			setting[j].start_time = queue_start_time;
			setting[j].on_time = taprio->entries[i].interval;
			setting[j].off_time = taprio->cycle_time - setting[j].on_time;
		}
		queue_start_time += taprio->entries[i].interval;
	}
	return 0;
}

static int phytium_set_taprio(struct net_device *ndev, struct queues_enst_setting *setting)
{
	struct macb *bp = netdev_priv(ndev);
	int i;
	int enst_ctrl = 0;
	int scale = 8;

	switch (bp->speed) {
	case SPEED_10:
		scale = 800;
		break;
	case SPEED_100:
		scale = 80;
		break;
	case SPEED_1000:
		scale = 8;
		break;
	default:
		pr_info("macb speed %d not support\n", bp->speed);
		return -EOPNOTSUPP;
	}

	for (i = 0; i < bp->num_queues; i++) {
		if (!setting[i].enable)
			continue;
		gem_writel_n(bp, ENST_START_TIME, i, setting[i].start_time);

		if (setting[i].on_time > 1048568 || setting[i].off_time > 1048568) {
			pr_warn("queue %d on_time:%d, off_time:%d should small 1048568 ns\n", i, setting[i].on_time, setting[i].off_time);
			return -EINVAL;
		}

		if (setting[i].on_time/scale < ndev->mtu + ETH_HLEN + ETH_FCS_LEN + NET_IP_ALIGN) {
			pr_info("tx frame may too long to be transmiited in on-time, it will cause transmission hang\n");
		}

		gem_writel_n(bp, ENST_ON_TIME, i, setting[i].on_time/scale);
		gem_writel_n(bp, ENST_OFF_TIME, i, setting[i].off_time/scale);
		enst_ctrl |= BIT(i);
	}
	gem_writel(bp, ENST_CONTROL, enst_ctrl);
	return 0;
}

static int phytium_set_taprio_tc(struct net_device *ndev, void *type_data)
{
	struct tc_taprio_qopt_offload *taprio = type_data;
	struct queues_enst_setting setting[MACB_MAX_ENST_QUEUES] = {0};
	int ret;

	_dump_taprio_param(taprio);

	ret = _taprio_convert_enst(taprio, setting);
	if (ret)
		return ret;

	_dump_enst_param(setting);

	ret = phytium_set_taprio(ndev, setting);

	return ret;
}

int phytium_setup_tc(struct net_device *ndev, enum tc_setup_type type, void *type_data)
{
	switch (type) {
	case TC_SETUP_QDISC_CBS:
		return phytium_set_cbs_tc(ndev, type_data);

	case TC_SETUP_QDISC_MQPRIO:
		return phytium_set_mqprio_tc(ndev, type_data);

	case TC_SETUP_QDISC_TAPRIO:
		return phytium_set_taprio_tc(ndev, type_data);

	default:
		return -EOPNOTSUPP;
	}
};

void phytium_init_tsn(struct platform_device *pdev)
{
	struct net_device *dev = platform_get_drvdata(pdev);
	struct macb *bp = netdev_priv(dev);

	if(!device_property_read_bool(&pdev->dev, "support-tsn")) {
		dev_dbg(&pdev->dev, "not support tsn, skip it");
		return;
	}

	bp->tsn_cap = TSN_CAP_8021Qav | TSN_CAP_8021Qbv;

	dev->hw_features |= NETIF_F_HW_TC;
}

