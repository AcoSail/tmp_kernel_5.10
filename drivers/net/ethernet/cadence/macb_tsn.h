/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * Phytium TSN support driver
 *
 * Copyright (C) 2023, Phytium Technology Co., Ltd.
 */
#ifndef _MACB_TSN_H
#define _MACB_TSN_H

#define MACB_MAX_ENST_QUEUES 8

#define TSN_CAP_8021Qav		0x00000001
#define TSN_CAP_8021Qbv		0x00000002
#define TSN_CAP_8021Qci		0x00000004
#define TSN_CAP_8021CB		0x00000008
#define TSN_CAP_8023br		0x00000010

/* 802.1Qav */
#define GEM_CBS_CONTROL				0x04BC
#define GEM_CBS_IDLESLOPE_Q_A		0x04C0
#define GEM_CBS_IDLESLOPE_Q_B		0x04C4

/* 802.1Qbv */
#define GEM_ENST_START_TIME			0x0800
#define GEM_ENST_ON_TIME			0x0820
#define GEM_ENST_OFF_TIME			0x0840
#define GEM_ENST_CONTROL			0x0880

#undef TSN_DEBUG
#ifdef TSN_DEBUG
#define _dump_param(param)    pr_err("%s %ld\n", #param, param)
#else
#define _dump_param(param)
#endif


void phytium_init_tsn(struct platform_device *pdev);
int phytium_setup_tc(struct net_device *ndev, enum tc_setup_type type, void *type_data);

#endif /* _MACB_TSN_H */
